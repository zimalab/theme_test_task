# Symfony & Twig theme test task

This project demonstrates template inheritance and theme possibilities using Symfony5, Twig and Sylius Theme Bundle.

## Requirements
* Git
* Symfony binary
* Composer
* PHP 8.0

## Installation
* Clone this project: 
  `git clone ...`
* Go to project directory: 
  `cd ./theme_test_task`
* Install dependencies: 
  `composer install`
* Copy theme public files to web root: 
  `bin/console sylius:theme:assets:install public`
* Run built-in symfony server:
  `symfony serve`
  
If installation succeed, test shops should be available in browser by this url: http://127.0.0.1:8000/  

## Implementation description
This app uses html, css and js files from https://en.endgamegear.de/ and https://en.noblechairs.de/ sites.

App contains 4 sample shops with different themes (just homepage and stub page).

Shops configuration is in `/config/services.yaml:7` file.

The desired shop is selected depending on the path prefix by `App\Event\Listener\ShopContextRequestListener`.

### Shop path prefixes:
* /default/: Default shop. Uses **shop/default** theme. 
* /mouse/: Mice shop. Uses **shop/mouse** theme which inherits **shop/default** 
* /chair/: Chairs shop. Uses **shop/chair** theme which inherits **shop/default** 
* /chair2/: Second chairs shop. Uses **inherited/chair** theme which inherits **shop/chair**

(There is a shop switch on each page footer)

### Themes

Main themes functionality provided by [SyliusThemeBundle](https://github.com/Sylius/SyliusThemeBundle).

All themes located in `/themes/` directory. Each theme:
* must have config file `composer.json` with defined name and parents
* may have static files (like images, css, js) in `public` subdirectory
* may have twig templates in `templates` subdirectory

Twig itself has many features helping avoid template code copy-paste and follow DRY principle: **extends**, **embed**, **use** and **include** tags. So it's possible to override any block of template in child template.
With themes, it's possible to (re-)define static files and templates from parent theme.