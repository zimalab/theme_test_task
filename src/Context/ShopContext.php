<?php

namespace App\Context;

use App\Exception\ShopNotFoundException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ShopContext
 * @package App\Context
 */
class ShopContext
{
    private string $shopCode;

    public function __construct(
        private RequestStack $requestStack,
        private array $shopConfigs = [],
    ) {
        $resolver = (new OptionsResolver())
            ->setRequired('prefix')
            ->setDefaults([
                'default' => false,
                'theme'   => 'shop/default',
            ])
            ->setAllowedTypes('prefix', 'string')
            ->setAllowedTypes('theme', 'string')
        ;

        foreach ($this->shopConfigs as $index => $config) {
            $this->shopConfigs[$index] = $resolver->resolve($config);
        }
    }

    public function getTheme(): string
    {
        return $this->getConfig()['theme'];
    }

    public function getPrefix(): string
    {
        return $this->getConfig()['prefix'];
    }

    /**
     * @return string|null
     */
    public function getShopCode(): ?string
    {
        return $this->shopCode;
    }

    public function resolveShopCode()
    {
        $code = $this->getShopCodeFromRequest() ?? $this->getDefaultShopCode();

        if (!$code) {
            throw new ShopNotFoundException('Could not resolve shop prefix and default prefix is not defined');
        }

        $this->shopCode = $code;
    }

    private function getShopCodeFromRequest(): ?string
    {
        $requestPrefix = $this->requestStack->getMainRequest()->get('_shop_prefix');

        return $requestPrefix ? $this->getShopCodeByPrefix($requestPrefix) : null;
    }

    private function getShopCodeByPrefix(string $prefix): ?string
    {
        foreach ($this->shopConfigs as $code => $config) {
            if ($config['prefix'] === $prefix) {
                return $code;
            }
        }

        return null;
    }

    private function getDefaultShopCode(): ?string
    {
        foreach ($this->shopConfigs as $code => $shopConfig) {
            if ($shopConfig['default'] === true) {
                return $code;
            }
        }

        return null;
    }

    private function getConfig(): array
    {
        return $this->shopConfigs[$this->shopCode];
    }
}