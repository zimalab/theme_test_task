<?php

namespace App\Controller;

use App\Context\ShopContext;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class StoreIndexController
 * @package App\Controller
 */
class ShopController extends AbstractController
{
    public function index(): Response
    {
        // Write custom logic for homepage here

        return $this->render('shop/index.html.twig');
    }

    /**
     * @param Request $request
     * @param ShopContext $context - contains shop-specific configuration
     * @return Response
     */
    public function stub(Request $request, ShopContext $context): Response
    {
        return $this->render('shop/stub.html.twig', [
            'shop'      => $context->getShopCode(),
            'theme'     => $context->getTheme(),
            'prefix'    => $context->getPrefix(),
            'page_path' => $request->get('path'),
        ]);
    }
}