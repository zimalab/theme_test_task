<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class IndexController
 * @package App\Controller
 */
class IndexController extends AbstractController
{
    public function index(): RedirectResponse
    {
        return $this->redirectToRoute('shop_index', ['_shop_prefix' => 'default']);
    }
}