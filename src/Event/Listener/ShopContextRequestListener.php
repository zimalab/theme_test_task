<?php

namespace App\Event\Listener;

use App\Context\ShopContext;
use App\Exception\ShopNotFoundException;
use Psr\Log\LoggerInterface;
use Sylius\Bundle\ThemeBundle\Context\SettableThemeContext;
use Sylius\Bundle\ThemeBundle\Repository\ThemeRepositoryInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class ShopContextRequestListener
 * @package App\Event\Listener
 */
class ShopContextRequestListener
{
    public function __construct(
        private ShopContext $shopContext,
        private SettableThemeContext $themeContext,
        private ThemeRepositoryInterface $themeRepository,
        private RouterInterface $router,
    ) {}

    public function onKernelRequest(RequestEvent $event): void
    {
        if (!$event->isMainRequest()) {
            // don't do anything if it's not the master request
            return;
        }

        try {
            $this->shopContext->resolveShopCode();

            $theme = $this->themeRepository->findOneByName($this->shopContext->getTheme());

            if ($theme) {
                $this->themeContext->setTheme($theme);
            }

            $this->router->getContext()->setParameter('_shop_prefix', $this->shopContext->getPrefix());
        } catch (ShopNotFoundException $exception) {
            if ($event->getRequest()->attributes->has('_shop_prefix')) {
                throw $exception;
            }
        }
    }
}