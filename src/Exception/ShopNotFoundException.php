<?php

namespace App\Exception;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ShopNotFoundException
 * @package App\Exception
 */
class ShopNotFoundException extends NotFoundHttpException implements ContextThrowable
{

}