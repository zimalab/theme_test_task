<?php

namespace App\Exception;

/**
 * Interface ContextThrowable
 * @package App\Exception
 */
interface ContextThrowable extends AppThrowable
{

}