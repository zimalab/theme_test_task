<?php

namespace App\Exception;

/**
 * Interface AppThrowable
 * @package App\Exception
 */
interface AppThrowable extends \Throwable
{

}